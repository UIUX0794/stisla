import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { Routes, RouterModule } from '@angular/router';
import { BaseLayoutComponent } from '../Layout/base-layout/base-layout.component';
import { MicrositeComponent } from './microsite/microsite.component';
import { LoyaltyComponent } from './loyalty/loyalty.component';
import { CustomerComponent } from './customer/customer.component';
import { PromotionComponent } from './promotion/promotion.component';
import { TransactionComponent } from './transaction/transaction.component';
import { TestimonialsComponent } from './testimonials/testimonials.component';
import { SettingsComponent } from './settings/settings.component';

const routes: Routes = [
  {
    path: '',
    component: BaseLayoutComponent,
    children: [
      { path: 'dashboard', component: DashboardComponent },
      { path: 'microsite', component: LoyaltyComponent },
      { path: 'loyalty', component: MicrositeComponent },
      { path: 'customer', component: CustomerComponent },
      { path: 'promotion', component: PromotionComponent },
      { path: 'transaction', component: TransactionComponent },
      { path: 'testimonials', component: TestimonialsComponent },
      { path: 'settings', component: SettingsComponent },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  declarations: [
    DashboardComponent,
    MicrositeComponent,
    LoyaltyComponent,
    CustomerComponent,
    PromotionComponent,
    TransactionComponent,
    TestimonialsComponent,
    SettingsComponent,
  ],
  imports: [CommonModule, RouterModule.forChild(routes)],
})
export class PagesModule {}
