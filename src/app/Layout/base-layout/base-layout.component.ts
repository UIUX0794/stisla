import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-base-layout',
  templateUrl: './base-layout.component.html',
  styleUrls: ['./base-layout.component.scss'],
})
export class BaseLayoutComponent implements OnInit {
  navBar = [
    {
      href: '/dashboard',
      label: 'Dashboard',
      class: ['fa fa-th-large'],
    },
    {
      href: '/microsite',
      label: 'Microsite',
      class: ['fa fa-server'],
    },
    {
      href: '/loyalty',
      label: 'Loyalty Program',
      class: ['fa fa-plus-circle'],
    },
    {
      href: '/customer',
      label: 'Customers',
      class: ['fa fa-user'],
    },
    {
      href: '/promotion',
      label: 'Promotion',
      class: ['fa fa-th-large'],
    },
    {
      href: '/transaction',
      label: 'Transactions',
      class: ['fa fa-comment'],
    },
    {
      href: '/testimonials',
      label: 'Testimonials',
      class: ['fa fa-comment'],
    },
    {
      href: '/settings',
      label: 'Settings',
      class: ['fa fa-cog'],
    },
  ];
  constructor() {}

  ngOnInit(): void {}

  // $button.addEventListener('click', (e) => {

  // });
  toggle() {
    console.log('hello toggle');
    const $button = document.querySelector('#sidebar-toggle');
    const $wrapper = document.querySelector('#wrapper'); //   e.preventDefault();
    $wrapper.classList.toggle('toggled');
  }
}
